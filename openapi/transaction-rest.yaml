openapi: "3.0.0"
info:
  version: 1.0.0
  title: Transaction REST
  description: RESTful service for account transactions
servers:
  - url: http://localhost:8080

paths:
  /admin/tenants:
    get:
      summary: List all tenants
      tags:
        - tenant
      responses:
        '200':
          description: An array of tenants
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Tenants"

  /admin/tenant:
    post:
      summary: Create a tenant
      tags:
        - tenant
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Tenant"
      responses:
        '201':
          description: Null response
        '400':
          description: Bad request
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
        '409':
          description: Already exists
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /admin/tenant/{tenantId}:
    get:
      summary: Get tenant
      tags:
        - tenant
      responses:
        '200':
          description: A tenant
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Tenant"
      parameters:
        - in: path
          name: tenantId
          required: true
          schema:
            type: string
            format: uuid
    patch:
      summary: Edit tenant
      tags:
        - tenant
      responses:
        '201':
          description: Edited tenant
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Tenant"
        '400':
          description: Bad request
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
      parameters:
        - in: path
          name: tenantId
          description: Tenant id
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Tenant"
    delete:
      summary: Delete tenant
      tags:
        - tenant
      responses:
        '204':
          description: Null response
        '400':
          description: Bad request
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
      parameters:
        - in: path
          name: tenantId
          description: Tenant id
          required: true
          schema:
            type: string


  /admin/users:
    get:
      summary: List all users
      tags:
        - user
      responses:
        '200':
          description: An array of users
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Users"
      parameters:
        - in: query
          name: tenantId
          description: Filter by tenant id
          schema:
            type: string
            format: uuid

  /admin/user:
    post:
      summary: Create a user
      tags:
        - user
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/User"

      responses:
        '201':
          description: Null response
        '400':
          description: Bad request
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
        '409':
          description: Already exists
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /admin/user/{userId}:
    get:
      summary: Get user
      tags:
        - user
      responses:
        '200':
          description: A user
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/User"
      parameters:
        - in: path
          name: userId
          required: true
          schema:
            type: string
            format: uuid
    patch:
      summary: Edit user
      tags:
        - user
      responses:
        '201':
          description: Edited user
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/User"
        '400':
          description: Bad request
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
      parameters:
        - in: path
          name: userId
          description: User id
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/User"
    delete:
      summary: Delete user
      tags:
        - user
      responses:
        '204':
          description: Null response
        '400':
          description: Bad request
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
      parameters:
        - in: path
          name: userId
          description: User id
          required: true
          schema:
            type: string



  /accounts:
    get:
      summary: List all accounts for user
      tags:
        - account
      responses:
        '200':
          description: An array of accounts
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Accounts"
      parameters:
        - in: query
          name: userId
          description: Filter by user id
          required: true
          schema:
            type: string
            format: uuid

  /account:
    post:
      summary: Create an account
      tags:
        - account
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Account"

      responses:
        '201':
          description: Null response
        '400':
          description: Bad request
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
        '409':
          description: Already exists
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /account/{accountId}:
    get:
      summary: Get account
      tags:
        - account
      responses:
        '200':
          description: An account
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/User"
      parameters:
        - in: path
          name: accountId
          required: true
          schema:
            type: string
            format: uuid

    patch:
      summary: Edit account
      tags:
        - account
      responses:
        '201':
          description: Edited account
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Account"
        '400':
          description: Bad request
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
      parameters:
        - in: path
          name: accountId
          description: Account id
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Account"
    delete:
      summary: Delete account
      tags:
        - account
      responses:
        '204':
          description: Null response
        '400':
          description: Bad request
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
      parameters:
        - in: path
          name: accountId
          description: Account id
          required: true
          schema:
            type: string


  /transaction:
    post:
      summary: Create transaction
      tags:
        - transaction
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Transaction"
      responses:
        '201':
          description: Null response
        '403':
          description: Forbidden
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
        '404':
          description: Bad request
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /transactions:
    get:
      summary: Get transactions for given user
      tags:
        - transaction
      parameters:
        - in: query
          name: accountId
          required: true
          schema:
            type: string
            format: uuid
        - in: query
          name: tenantId
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          description: List of transactions
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Transactions"

  /transaction/{transactionId}:
    get:
      summary: Get transaction details
      tags:
        - transaction
      responses:
        '200':
          description: Transaction details
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Transaction"
      parameters:
        - in: path
          name: transactionId
          required: true
          schema:
            type: string



components:
  schemas:
    Tenant:
      type: object
      required:
        - name
      properties:
        id:
          type: string
          format: uuid
        name:
          type: string
    Tenants:
      type: array
      items:
        $ref: "#/components/schemas/Tenant"


    User:
      type: object
      required:
        - tenantId
      properties:
        id:
          type: string
          format: uuid
        email:
          type: string
          format: email
        tenantId:
          type: string
          format: uuid
    Users:
      type: array
      items:
        $ref: "#/components/schemas/User"

    Account:
      type: object
      required:
        - name
        - userId
        - currency
      properties:
        id:
          type: string
          format: uuid
        name:
          type: string
        userId:
          type: string
          format: uuid
        currency:
          type: string
        balance:
          type: number
          format: currency
          default: 0.0
    Accounts:
      type: array
      items:
        $ref: "#/components/schemas/Account"

    Transaction:
      type: object
      required:
        - to
        - from
        - amount
        - currency
      properties:
        id:
          type: string
        to:
          type: string
          format: uuid
        from:
          type: string
          format: uuid
        amount:
          type: number
          format: currency
        currency:
          type: string
          format: currency
        note:
          type: string
    Transactions:
      type: array
      items:
        $ref: "#/components/schemas/Transaction"


    Error:
      type: object
      required:
        - code
        - message
      properties:
        code:
          type: integer
          format: int32
        message:
          type: string
