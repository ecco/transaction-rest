package com.transaction.rest;

import com.transaction.rest.core.api.*;
import org.apache.log4j.Logger;
import org.wso2.msf4j.MicroservicesRunner;

/**
 * Application entry point.
 *
 * @since 1.0.0-SNAPSHOT
 */
public class Application {
    private static final Logger logger = Logger.getLogger(Application.class.getName());

    public static void main(String[] args) throws Exception {
        // Register db driver
        Class.forName("org.h2.Driver");
        logger.info("registered database driver");

        logger.info("starting Micro Services");
        new MicroservicesRunner(Integer.getInteger("http.port", 8080))
                .deploy(new AccountApi(),
                        new AccountsApi(),
                        new AdminApi(),
                        new TransactionApi(),
                        new TransactionsApi())
                .start();
    }
}
