package com.transaction.rest.infrastructure.database;

import static java.lang.Boolean.TRUE;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import dagger.Module;
import dagger.Provides;
import javax.sql.DataSource;

@Module
public class ConnectionPoolConfig {

  @Provides
  static DataSource provideDataSource() {
    HikariConfig conf = new HikariConfig();
    conf.setPoolName("hikariConnectionPool");
    conf.setMaximumPoolSize(Integer.getInteger("cp.pool.size.maximum", 1000));
    conf.setJdbcUrl(System.getProperty("jdbc.url", "jdbc:h2:~/database"));
    conf.setUsername(System.getProperty("jdbc.username", "sa"));
    conf.setPassword(System.getProperty("jdbc.password", ""));

    conf.addDataSourceProperty("cachePrepStmts", TRUE);
    conf.addDataSourceProperty("useServerPrepStmts", TRUE);

    return new HikariDataSource(conf);
  }
}
