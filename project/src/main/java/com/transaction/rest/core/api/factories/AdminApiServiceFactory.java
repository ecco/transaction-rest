package com.transaction.rest.core.api.factories;

import com.transaction.rest.core.api.AdminApiService;
import com.transaction.rest.core.api.impl.AdminApiServiceImpl;

public class AdminApiServiceFactory {
    private final static AdminApiService service = new AdminApiServiceImpl();

    public static AdminApiService getAdminApi() {
        return service;
    }
}
