package com.transaction.rest.core.api.tenant.persistence;

import com.transaction.rest.core.api.model.Tenant;
import dagger.Module;
import java.util.List;
import javax.inject.Inject;
import javax.sql.DataSource;

@Module
public  class TenantDAO {
  private DataSource dataSource;

  @Inject
  public TenantDAO(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  public List<Tenant> getTenants() {
    return null;
  }
}
