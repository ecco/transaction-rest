package com.transaction.rest.core.api.factories;

import com.transaction.rest.core.api.TransactionApiService;
import com.transaction.rest.core.api.impl.TransactionApiServiceImpl;

public class TransactionApiServiceFactory {
    private final static TransactionApiService service = new TransactionApiServiceImpl();

    public static TransactionApiService getTransactionApi() {
        return service;
    }
}
