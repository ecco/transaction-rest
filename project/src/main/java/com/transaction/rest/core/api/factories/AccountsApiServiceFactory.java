package com.transaction.rest.core.api.factories;

import com.transaction.rest.core.api.AccountsApiService;
import com.transaction.rest.core.api.impl.AccountsApiServiceImpl;

public class AccountsApiServiceFactory {
    private final static AccountsApiService service = new AccountsApiServiceImpl();

    public static AccountsApiService getAccountsApi() {
        return service;
    }
}
