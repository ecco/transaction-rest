package com.transaction.rest.core.api.impl;

import com.transaction.rest.core.api.*;
import com.transaction.rest.core.api.model.*;

import com.transaction.rest.core.api.model.Error;
import com.transaction.rest.core.api.model.Tenant;

import java.util.UUID;

import com.transaction.rest.core.api.model.User;

import java.util.List;

import com.transaction.rest.core.api.NotFoundException;

import java.io.InputStream;

import org.wso2.msf4j.formparam.FormDataParam;
import org.wso2.msf4j.formparam.FileInfo;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class AdminApiServiceImpl extends AdminApiService {

  @Override
  public Response adminTenantPost(Tenant tenant) throws NotFoundException {

    return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
  }

  @Override
  public Response adminTenantTenantIdDelete(String tenantId) throws NotFoundException {
    // do some magic!
    return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "")).build();
  }

  @Override
  public Response adminTenantTenantIdGet(UUID tenantId
  ) throws NotFoundException {
    // do some magic!
    return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
  }

  @Override
  public Response adminTenantTenantIdPatch(String tenantId
      , Tenant tenant
  ) throws NotFoundException {
    // do some magic!
    return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
  }

  @Override
  public Response adminTenantsGet() throws NotFoundException {
    // do some magic!
    return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
  }

  @Override
  public Response adminUserPost(User user
  ) throws NotFoundException {
    // do some magic!
    return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
  }

  @Override
  public Response adminUserUserIdDelete(String userId
  ) throws NotFoundException {
    // do some magic!
    return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
  }

  @Override
  public Response adminUserUserIdGet(UUID userId
  ) throws NotFoundException {
    // do some magic!
    return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
  }

  @Override
  public Response adminUserUserIdPatch(String userId
      , User user
  ) throws NotFoundException {
    // do some magic!
    return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
  }

  @Override
  public Response adminUsersGet(UUID tenantId
  ) throws NotFoundException {
    // do some magic!
    return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
  }
}
