package com.transaction.rest.core.api.factories;

import com.transaction.rest.core.api.TransactionsApiService;
import com.transaction.rest.core.api.impl.TransactionsApiServiceImpl;

public class TransactionsApiServiceFactory {
    private final static TransactionsApiService service = new TransactionsApiServiceImpl();

    public static TransactionsApiService getTransactionsApi() {
        return service;
    }
}
