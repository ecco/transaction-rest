package com.transaction.rest.core.api.impl;

import com.transaction.rest.core.api.*;
import com.transaction.rest.core.api.model.*;

import com.transaction.rest.core.api.model.Account;
import java.util.UUID;

import java.util.List;
import com.transaction.rest.core.api.NotFoundException;

import java.io.InputStream;

import org.wso2.msf4j.formparam.FormDataParam;
import org.wso2.msf4j.formparam.FileInfo;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaMSF4JServerCodegen", date = "2019-08-08T22:31:05.017967+03:00[Europe/Bucharest]")
public class AccountsApiServiceImpl extends AccountsApiService {
    @Override
    public Response accountsGet(UUID userId
 ) throws NotFoundException {
        // do some magic!
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
    }
}
