package com.transaction.rest.core.api.factories;

import com.transaction.rest.core.api.AccountApiService;
import com.transaction.rest.core.api.impl.AccountApiServiceImpl;

public class AccountApiServiceFactory {
    private final static AccountApiService service = new AccountApiServiceImpl();

    public static AccountApiService getAccountApi() {
        return service;
    }
}
