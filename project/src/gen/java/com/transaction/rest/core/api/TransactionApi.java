package com.transaction.rest.core.api;

import com.transaction.rest.core.api.model.*;
import com.transaction.rest.core.api.TransactionApiService;
import com.transaction.rest.core.api.factories.TransactionApiServiceFactory;

import io.swagger.annotations.ApiParam;
import io.swagger.jaxrs.*;

import com.transaction.rest.core.api.model.Error;
import com.transaction.rest.core.api.model.Transaction;

import java.util.List;
import com.transaction.rest.core.api.NotFoundException;

import java.io.InputStream;

import org.wso2.msf4j.formparam.FormDataParam;
import org.wso2.msf4j.formparam.FileInfo;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.*;

@Path("/transaction")


@io.swagger.annotations.Api(description = "the transaction API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaMSF4JServerCodegen", date = "2019-08-08T22:31:05.017967+03:00[Europe/Bucharest]")
public class TransactionApi  {
   private final TransactionApiService delegate = TransactionApiServiceFactory.getTransactionApi();

    @POST
    
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "Create transaction", notes = "", response = Void.class, tags={ "transaction", })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 201, message = "Null response", response = Void.class),
        
        @io.swagger.annotations.ApiResponse(code = 403, message = "Forbidden", response = Void.class),
        
        @io.swagger.annotations.ApiResponse(code = 404, message = "Bad request", response = Void.class) })
    public Response transactionPost(@ApiParam(value = "" ) Transaction transaction
)
    throws NotFoundException {
        return delegate.transactionPost(transaction);
    }
    @GET
    @Path("/{transactionId}")
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "Get transaction details", notes = "", response = Transaction.class, tags={ "transaction", })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 200, message = "Transaction details", response = Transaction.class) })
    public Response transactionTransactionIdGet(@ApiParam(value = "",required=true) @PathParam("transactionId") String transactionId
)
    throws NotFoundException {
        return delegate.transactionTransactionIdGet(transactionId);
    }
}
