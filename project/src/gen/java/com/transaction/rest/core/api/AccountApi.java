package com.transaction.rest.core.api;

import com.transaction.rest.core.api.model.*;
import com.transaction.rest.core.api.AccountApiService;
import com.transaction.rest.core.api.factories.AccountApiServiceFactory;

import io.swagger.annotations.*;
import io.swagger.jaxrs.*;

import com.transaction.rest.core.api.model.Account;
import com.transaction.rest.core.api.model.Error;

import java.util.UUID;

import com.transaction.rest.core.api.model.User;

import java.util.List;

import com.transaction.rest.core.api.NotFoundException;

import java.io.InputStream;

import org.wso2.msf4j.formparam.FormDataParam;
import org.wso2.msf4j.formparam.FileInfo;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.*;

@Path("/account")


@Api(description = "the account API")
public class AccountApi {
    private final AccountApiService delegate = AccountApiServiceFactory.getAccountApi();

    @DELETE
    @Path("/{accountId}")

    @Produces({"application/json"})
    @ApiOperation(value = "Delete account", notes = "", response = Void.class, tags = {"account",})
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Null response", response = Void.class),

            @ApiResponse(code = 400, message = "Bad request", response = Void.class)})
    public Response accountAccountIdDelete(@ApiParam(value = "Account id", required = true) @PathParam("accountId") String accountId
    )
            throws NotFoundException {
        return delegate.accountAccountIdDelete(accountId);
    }

    @GET
    @Path("/{accountId}")

    @Produces({"application/json"})
    @ApiOperation(value = "Get account", notes = "", response = User.class, tags = {"account",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "An account", response = User.class)})
    public Response accountAccountIdGet(@ApiParam(value = "", required = true) @PathParam("accountId") UUID accountId
    )
            throws NotFoundException {
        return delegate.accountAccountIdGet(accountId);
    }

    @PATCH
    @Path("/{accountId}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "Edit account", notes = "", response = Account.class, tags = {"account",})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Edited account", response = Account.class),

            @ApiResponse(code = 400, message = "Bad request", response = Account.class)})
    public Response accountAccountIdPatch(@ApiParam(value = "Account id", required = true) @PathParam("accountId") String accountId
            , @ApiParam(value = "") Account account
    )
            throws NotFoundException {
        return delegate.accountAccountIdPatch(accountId, account);
    }

    @POST

    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "Create an account", notes = "", response = Void.class, tags = {"account",})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Null response", response = Void.class),

            @ApiResponse(code = 400, message = "Bad request", response = Void.class),

            @ApiResponse(code = 409, message = "Already exists", response = Void.class)})
    public Response accountPost(@ApiParam(value = "") Account account
    )
            throws NotFoundException {
        return delegate.accountPost(account);
    }
}
