package com.transaction.rest.core.api;

import com.transaction.rest.core.api.model.*;
import com.transaction.rest.core.api.AccountsApiService;
import com.transaction.rest.core.api.factories.AccountsApiServiceFactory;

import io.swagger.annotations.ApiParam;
import io.swagger.jaxrs.*;

import com.transaction.rest.core.api.model.Account;
import java.util.UUID;

import java.util.List;
import com.transaction.rest.core.api.NotFoundException;

import java.io.InputStream;

import org.wso2.msf4j.formparam.FormDataParam;
import org.wso2.msf4j.formparam.FileInfo;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.*;

@Path("/accounts")


@io.swagger.annotations.Api(description = "the accounts API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaMSF4JServerCodegen", date = "2019-08-08T22:31:05.017967+03:00[Europe/Bucharest]")
public class AccountsApi  {
   private final AccountsApiService delegate = AccountsApiServiceFactory.getAccountsApi();

    @GET
    
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "List all accounts for user", notes = "", response = Account.class, responseContainer = "List", tags={ "account", })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 200, message = "An array of accounts", response = Account.class, responseContainer = "List") })
    public Response accountsGet(@ApiParam(value = "Filter by user id",required=true) @QueryParam("userId") UUID userId
)
    throws NotFoundException {
        return delegate.accountsGet(userId);
    }
}
