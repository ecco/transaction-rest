package com.transaction.rest.core.api;

import com.transaction.rest.core.api.*;
import com.transaction.rest.core.api.model.*;

import org.wso2.msf4j.formparam.FormDataParam;
import org.wso2.msf4j.formparam.FileInfo;

import com.transaction.rest.core.api.model.Error;
import com.transaction.rest.core.api.model.Transaction;

import java.util.List;
import com.transaction.rest.core.api.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaMSF4JServerCodegen", date = "2019-08-08T22:31:05.017967+03:00[Europe/Bucharest]")
public abstract class TransactionApiService {
    public abstract Response transactionPost(Transaction transaction
 ) throws NotFoundException;
    public abstract Response transactionTransactionIdGet(String transactionId
 ) throws NotFoundException;
}
