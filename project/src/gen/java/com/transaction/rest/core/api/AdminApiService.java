package com.transaction.rest.core.api;

import com.transaction.rest.core.api.*;
import com.transaction.rest.core.api.model.*;

import org.wso2.msf4j.formparam.FormDataParam;
import org.wso2.msf4j.formparam.FileInfo;

import com.transaction.rest.core.api.model.Error;
import com.transaction.rest.core.api.model.Tenant;
import java.util.UUID;
import com.transaction.rest.core.api.model.User;

import java.util.List;
import com.transaction.rest.core.api.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaMSF4JServerCodegen", date = "2019-08-08T22:31:05.017967+03:00[Europe/Bucharest]")
public abstract class AdminApiService {
    public abstract Response adminTenantPost(Tenant tenant
 ) throws NotFoundException;
    public abstract Response adminTenantTenantIdDelete(String tenantId
 ) throws NotFoundException;
    public abstract Response adminTenantTenantIdGet(UUID tenantId
 ) throws NotFoundException;
    public abstract Response adminTenantTenantIdPatch(String tenantId
 ,Tenant tenant
 ) throws NotFoundException;
    public abstract Response adminTenantsGet() throws NotFoundException;
    public abstract Response adminUserPost(User user
 ) throws NotFoundException;
    public abstract Response adminUserUserIdDelete(String userId
 ) throws NotFoundException;
    public abstract Response adminUserUserIdGet(UUID userId
 ) throws NotFoundException;
    public abstract Response adminUserUserIdPatch(String userId
 ,User user
 ) throws NotFoundException;
    public abstract Response adminUsersGet(UUID tenantId
 ) throws NotFoundException;
}
