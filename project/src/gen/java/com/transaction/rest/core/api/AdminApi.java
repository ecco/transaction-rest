package com.transaction.rest.core.api;

import com.transaction.rest.core.api.model.*;
import com.transaction.rest.core.api.AdminApiService;
import com.transaction.rest.core.api.factories.AdminApiServiceFactory;

import io.swagger.annotations.*;
import io.swagger.jaxrs.*;

import com.transaction.rest.core.api.model.Error;
import com.transaction.rest.core.api.model.Tenant;
import java.util.UUID;
import com.transaction.rest.core.api.model.User;

import java.util.List;
import com.transaction.rest.core.api.NotFoundException;

import java.io.InputStream;

import org.wso2.msf4j.formparam.FormDataParam;
import org.wso2.msf4j.formparam.FileInfo;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.*;

@Path("/admin")


@Api(description = "the admin API")
public class AdminApi  {
   private final AdminApiService delegate = AdminApiServiceFactory.getAdminApi();

    @POST
    @Path("/tenant")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "Create a tenant", notes = "", response = Void.class, tags={ "tenant", })
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Null response", response = Void.class),
        
        @ApiResponse(code = 400, message = "Bad request", response = Void.class),
        
        @ApiResponse(code = 409, message = "Already exists", response = Void.class) })
    public Response adminTenantPost(@ApiParam(value = "" ) Tenant tenant
)
    throws NotFoundException {
        return delegate.adminTenantPost(tenant);
    }
    @DELETE
    @Path("/tenant/{tenantId}")
    
    @Produces({ "application/json" })
    @ApiOperation(value = "Delete tenant", notes = "", response = Void.class, tags={ "tenant", })
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Null response", response = Void.class),
        
        @ApiResponse(code = 400, message = "Bad request", response = Void.class) })
    public Response adminTenantTenantIdDelete(@ApiParam(value = "Tenant id",required=true) @PathParam("tenantId") String tenantId
)
    throws NotFoundException {
        return delegate.adminTenantTenantIdDelete(tenantId);
    }
    @GET
    @Path("/tenant/{tenantId}")
    
    @Produces({ "application/json" })
    @ApiOperation(value = "Get tenant", notes = "", response = Tenant.class, tags={ "tenant", })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "A tenant", response = Tenant.class) })
    public Response adminTenantTenantIdGet(@ApiParam(value = "",required=true) @PathParam("tenantId") UUID tenantId
)
    throws NotFoundException {
        return delegate.adminTenantTenantIdGet(tenantId);
    }
    @PATCH
    @Path("/tenant/{tenantId}")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "Edit tenant", notes = "", response = Tenant.class, tags={ "tenant", })
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Edited tenant", response = Tenant.class),
        
        @ApiResponse(code = 400, message = "Bad request", response = Tenant.class) })
    public Response adminTenantTenantIdPatch(@ApiParam(value = "Tenant id",required=true) @PathParam("tenantId") String tenantId
,@ApiParam(value = "" ) Tenant tenant
)
    throws NotFoundException {
        return delegate.adminTenantTenantIdPatch(tenantId,tenant);
    }
    @GET
    @Path("/tenants")
    
    @Produces({ "application/json" })
    @ApiOperation(value = "List all tenants", notes = "", response = Tenant.class, responseContainer = "List", tags={ "tenant", })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "An array of tenants", response = Tenant.class, responseContainer = "List") })
    public Response adminTenantsGet()
    throws NotFoundException {
        return delegate.adminTenantsGet();
    }
    @POST
    @Path("/user")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "Create a user", notes = "", response = Void.class, tags={ "user", })
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Null response", response = Void.class),
        
        @ApiResponse(code = 400, message = "Bad request", response = Void.class),
        
        @ApiResponse(code = 409, message = "Already exists", response = Void.class) })
    public Response adminUserPost(@ApiParam(value = "" ) User user
)
    throws NotFoundException {
        return delegate.adminUserPost(user);
    }
    @DELETE
    @Path("/user/{userId}")
    
    @Produces({ "application/json" })
    @ApiOperation(value = "Delete user", notes = "", response = Void.class, tags={ "user", })
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Null response", response = Void.class),
        
        @ApiResponse(code = 400, message = "Bad request", response = Void.class) })
    public Response adminUserUserIdDelete(@ApiParam(value = "User id",required=true) @PathParam("userId") String userId
)
    throws NotFoundException {
        return delegate.adminUserUserIdDelete(userId);
    }
    @GET
    @Path("/user/{userId}")
    
    @Produces({ "application/json" })
    @ApiOperation(value = "Get user", notes = "", response = User.class, tags={ "user", })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "A user", response = User.class) })
    public Response adminUserUserIdGet(@ApiParam(value = "",required=true) @PathParam("userId") UUID userId
)
    throws NotFoundException {
        return delegate.adminUserUserIdGet(userId);
    }
    @PATCH
    @Path("/user/{userId}")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "Edit user", notes = "", response = User.class, tags={ "user", })
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Edited user", response = User.class),
        
        @ApiResponse(code = 400, message = "Bad request", response = User.class) })
    public Response adminUserUserIdPatch(@ApiParam(value = "User id",required=true) @PathParam("userId") String userId
,@ApiParam(value = "" ) User user
)
    throws NotFoundException {
        return delegate.adminUserUserIdPatch(userId,user);
    }
    @GET
    @Path("/users")
    
    @Produces({ "application/json" })
    @ApiOperation(value = "List all users", notes = "", response = User.class, responseContainer = "List", tags={ "user", })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "An array of users", response = User.class, responseContainer = "List") })
    public Response adminUsersGet(@ApiParam(value = "Filter by tenant id") @QueryParam("tenantId") UUID tenantId
)
    throws NotFoundException {
        return delegate.adminUsersGet(tenantId);
    }
}
