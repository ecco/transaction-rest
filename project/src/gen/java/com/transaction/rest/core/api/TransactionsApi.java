package com.transaction.rest.core.api;

import com.transaction.rest.core.api.model.*;
import com.transaction.rest.core.api.TransactionsApiService;
import com.transaction.rest.core.api.factories.TransactionsApiServiceFactory;

import io.swagger.annotations.ApiParam;
import io.swagger.jaxrs.*;

import com.transaction.rest.core.api.model.Transaction;
import java.util.UUID;

import java.util.List;
import com.transaction.rest.core.api.NotFoundException;

import java.io.InputStream;

import org.wso2.msf4j.formparam.FormDataParam;
import org.wso2.msf4j.formparam.FileInfo;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.*;

@Path("/transactions")


@io.swagger.annotations.Api(description = "the transactions API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaMSF4JServerCodegen", date = "2019-08-08T22:31:05.017967+03:00[Europe/Bucharest]")
public class TransactionsApi  {
   private final TransactionsApiService delegate = TransactionsApiServiceFactory.getTransactionsApi();

    @GET
    
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "Get transactions for given user", notes = "", response = Transaction.class, responseContainer = "List", tags={ "transaction", })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 200, message = "List of transactions", response = Transaction.class, responseContainer = "List") })
    public Response transactionsGet(@ApiParam(value = "",required=true) @QueryParam("accountId") UUID accountId
,@ApiParam(value = "",required=true) @QueryParam("tenantId") UUID tenantId
)
    throws NotFoundException {
        return delegate.transactionsGet(accountId,tenantId);
    }
}
